﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeScene : MonoBehaviour
{
    public void loadLevel(int color)
    {
        GameManager.Instance.color = color;
        SceneManager.LoadScene(GameManager.Instance.nivellColors[color]);
    }

    public void repteTotal()
    {
        GameManager.Instance.Repte = true;
        SceneManager.LoadScene(GameManager.Instance.nivellColors[GameManager.Instance.color]);
    }

    public void loadScene(string scene)
    {
        SceneManager.LoadScene(scene);
    }
}
