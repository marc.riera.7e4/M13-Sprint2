﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextNivellProgressiu : MonoBehaviour
{


    // Update is called once per frame
    void Update()
    {

        GameObject.Find("TextNivellProgressiu").GetComponent<Text>().text = (GameManager.Instance.Stage + 1) + " / 20";
    }
}
