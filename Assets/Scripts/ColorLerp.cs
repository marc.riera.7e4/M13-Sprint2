﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorLerp : MonoBehaviour
{
    int num = 0;

    SpriteRenderer sp;
    [SerializeField] [Range(0f, 1f)] float lerpTime = 1f;
    [SerializeField] Color[] myColors = {  /*new Color(0.5f, 0.5f, 0.5f, 1), */ new Color(0.3f, 0.3f, 0.3f, 1f), new Color(0.6f, 0.6f, 0.6f, 1f) };

    float rand;

    int colorIndex = Random.Range(0, 1);

    float t = 0f;

    int len;

    void Start()
    {
        sp = GetComponent<SpriteRenderer>();
        len = myColors.Length;
        rand = Random.Range(0.5f, 1f);

    }

    void Update()
    {
        num++;
        sp.material.color = Color.Lerp(sp.material.color, myColors[colorIndex], rand * Time.deltaTime);

        t = Mathf.Lerp(t, 1f, rand * Time.deltaTime);
        if (t > .9f)
        {
            t = 0f;
            colorIndex++;
            colorIndex = (colorIndex >= len) ? 0 : colorIndex;
            //colorIndex = Random.Range(0, 3);
        }
    }
}
