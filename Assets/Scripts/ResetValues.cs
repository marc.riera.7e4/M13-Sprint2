﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetValues : MonoBehaviour
{
    void Start()
    {
        GameManager.Instance.Stage = 0;
        GameManager.Instance.Vidas = 1;
        GameManager.Instance.Repte = false;
        GameManager.Instance.color = 0;
        for (int x = 0; x < GameManager.Instance.resultats.Length; x++)
        {
            GameManager.Instance.resultats[x] = 0;
        }
    }
}
