﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ClickSprite : MonoBehaviour
{

    //string scene = SceneManager.GetActiveScene.name;
    public GameObject nextLvlButton;
    public GameObject quitButton;
    void Start()
    {
        var scene = SceneManager.GetActiveScene().name;
        if (scene == "Result") resultButtonToggler();
    }


    void OnMouseDown()
    {
        Scene scene = SceneManager.GetActiveScene();
        if (this.name == "CenterClickable")
        {

            GameManager.Instance.reload();
            GameManager.Instance.PlaySound(0);
            if (GameManager.Instance.Stage == 20)
            {
                GameManager.Instance.resultats[GameManager.Instance.color] = GameManager.Instance.Stage;
                SceneManager.LoadScene("Result");
            }
            else if (GameManager.Instance.Repte == true && GameManager.Instance.color == 0)
            {
                GameManager.Instance.resultats[GameManager.Instance.color] = GameManager.Instance.Stage;
                SceneManager.LoadScene("Result");
            }
        }
        else
        {
            GameManager.Instance.PlaySound(1);
            if (GameManager.Instance.Vidas == 0 && GameManager.Instance.Repte == true)
            {
                GameManager.Instance.resultats[GameManager.Instance.color] = GameManager.Instance.Stage;
                SceneManager.LoadScene("Result");
            }
            else if (GameManager.Instance.Vidas == 0)
            {
                GameManager.Instance.resultats[GameManager.Instance.color] = GameManager.Instance.Stage;
                // POSAR PANTALLA RESULTATS
                GameManager.Instance.Stage = 0;
                GameManager.Instance.Vidas = 1;
                SceneManager.LoadScene("Result");
            }
            else
            {

                GameManager.Instance.Vidas--;
            }


        }
    }

    public void SurrenderButton()
    {
        if (GameManager.Instance.Repte == true)
        {
            GameManager.Instance.resultats[GameManager.Instance.color] = GameManager.Instance.Stage;
            GameManager.Instance.nextColor();
        }
        else
        {
            GameManager.Instance.resultats[GameManager.Instance.color] = GameManager.Instance.Stage;
            SceneManager.LoadScene("Inici");
        }
    }

    public void QuitButton()
    {
        SceneManager.LoadScene("Inici");
    }

    public void NextLvlButton()
    {
        GameManager.Instance.nextColor();
    }

    void resultButtonToggler()
    {
        if (GameManager.Instance.Repte == true)
        {
            nextLvlButton.gameObject.SetActive(true);
            quitButton.gameObject.SetActive(false);
        }
    }
}
