﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour
{
    public Sound[] sounds;
    private AudioSource controlAudio;
    // Start is called before the first frame update
    void Awake()
    {
        controlAudio = GameObject.Find("AudioManager").GetComponent<AudioSource>();
    }

    // Update is called once per frame
    public void SeleccionAudio(int indice)
    {
        controlAudio.PlayOneShot(sounds[indice].clip, sounds[indice].volume);
        
    }

    
}
