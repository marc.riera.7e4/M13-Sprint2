﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{


    public static GameManager _instance;
    public static GameManager Instance { get { return _instance; } }

    public int Stage = 0;
    public int Vidas = 1;
    public bool Repte = false;
    [SerializeField]
    public int color = 0;

    [SerializeField]
    public string[] nivellColors;

    [SerializeField]
    public int[] resultats;

    private AudioManager audioManager;

    

    private void Awake()
    {
        if (_instance == null)
        {
            DontDestroyOnLoad(gameObject);
            _instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        audioManager = GameObject.Find("AudioManager").GetComponent<AudioManager>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void reload()
    {
        Stage++;
        var scene = SceneManager.GetActiveScene().name;
        SceneManager.LoadScene(scene);
    }

    public void nextColor()
    {
        
        
        Stage = 0;
        Vidas = 1;
        color++;
        if (color == 4 && Repte == true)
        {
            SceneManager.LoadScene("UiResultatFinal");
        }

        else if (color == 4)
        {
            color = 0;
            Repte = false;
            SceneManager.LoadScene("Inici");

        }
        else
        {
            SceneManager.LoadScene(nivellColors[color]);

        }

    }

    public void PlaySound(int indice)
    {
        audioManager.SeleccionAudio(indice);

    }
}
