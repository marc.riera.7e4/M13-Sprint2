﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridManagerMarc : MonoBehaviour
{
    [SerializeField]
    public Sprite referenceTile;
    [SerializeField]
    private int rows = 16;
    [SerializeField]
    private int cols = 16;
    [SerializeField]
    public float tilesize;
    [SerializeField]
    public float transparencia;

    [SerializeField]
    public Color[] colorsMatriu;

    [SerializeField]
    public Color[] colorsZona;
    int ver, hor;

    void Start()
    {
        ver = (int)Camera.main.orthographicSize;
        hor = ver * (Screen.width / Screen.height);
        GameObject.Find("GameObject").transform.position = new Vector2(7.5f * tilesize, 4);
        for (int i = 0; i < cols; i++)
        {
            for (int j = 0; j < rows; j++)
            {
                
                float rand = Random.Range(0.1f, 0.4f);
                GenerateGrid(i, j, rand);
            }
        }
        GenerateNewTile();
        //StartCoroutine("Animacio");

    }

    private void GenerateGrid(int x, int y, float val)
    {
        GameObject g = new GameObject("x: " + x + "y: " + y);

        g.transform.position = transform.position - (new Vector3(x * tilesize, y * tilesize));
        var s = g.AddComponent<SpriteRenderer>();
        g.AddComponent<ClickSprite>();
        g.AddComponent<BoxCollider2D>();
        g.AddComponent<ColorLerp>();
        g.GetComponent<BoxCollider2D>().size = new Vector2(1, 1);
        s.sprite = referenceTile;
        s.transform.localScale = new Vector2(tilesize, tilesize);
        var ran = Random.Range(0.3f, -0.3f);
        s.color = new Color(colorsMatriu[GameManager.Instance.color].r+ran, colorsMatriu[GameManager.Instance.color].g + ran, colorsMatriu[GameManager.Instance.color].b + ran);
    }

    private void GenerateNewTile()
    {
        GameObject g = new GameObject("CenterClickable");
        int centerPOSx = Random.Range(1, cols - 1);
        int centerPOSy = Random.Range(1, rows - 1);
        g.transform.position = transform.position - (new Vector3(centerPOSx * tilesize, centerPOSy * tilesize, 1));
        var s = g.AddComponent<SpriteRenderer>();
        g.AddComponent<ClickSprite>();
        g.AddComponent<BoxCollider2D>();

        g.GetComponent<BoxCollider2D>().size = new Vector2(1, 1);
        s.sprite = referenceTile;

        float rand = Random.Range(0.1f, 0.5f);

        if (GameManager.Instance.color == 0)
        {
            s.color = new Color(colorsZona[0].r, colorsZona[0].g, colorsZona[0].b, transparencia - (0.03f * GameManager.Instance.Stage));
        } else if (GameManager.Instance.color == 1)
        {
            s.color = new Color(colorsZona[1].r, colorsZona[1].g, colorsZona[1].b, transparencia - (0.03f * GameManager.Instance.Stage));
        }
        else if (GameManager.Instance.color == 2)
        {
            s.color = new Color(colorsZona[2].r, colorsZona[2].g, colorsZona[2].b, transparencia - (0.03f * GameManager.Instance.Stage));
        }
        else if (GameManager.Instance.color == 3)
        {
            s.color = new Color(colorsZona[3].r, colorsZona[3].g, colorsZona[3].b, transparencia - (0.03f * GameManager.Instance.Stage));
        }
        Vector3 objectScale = transform.localScale;
        objectScale.x = tilesize * 3;
        objectScale.y = tilesize * 3;
        s.transform.localScale = objectScale;
    }

    /*IEnumerator Animacio()
    {
        while (true)
        {
            

            for (int i = 0; i < cols; i++)
            {
                for (int j = 0; j < rows; j++)
                {
                    Color a = new Color(1, 1, 1);
                    GameObject.Find("x: " + i + "y: " + j).GetComponent<SpriteRenderer>().material.color = Color.Lerp(GameObject.Find("x: " + i + "y: " + j).GetComponent<SpriteRenderer>().material.color, a, 1);
                }
            }
            
            yield return new WaitForSeconds(0.1f);
            Debug.Log("A");
        }
    }*/

    
}

