﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextResultat : MonoBehaviour
{
    public int color;

    public string textColor;

    void Start()
    {
        if (color == 5)
        {
            Debug.Log(GameManager.Instance.color);
            Debug.Log(GameManager.Instance.resultats[GameManager.Instance.color]);
            gameObject.GetComponentInChildren<Text>().text = GameManager.Instance.resultats[GameManager.Instance.color] + " / 20";
        }
        else
        {
            this.gameObject.GetComponentInChildren<Text>().text = textColor + ": " + GameManager.Instance.resultats[color] + " / 20";
        }
    }
}
