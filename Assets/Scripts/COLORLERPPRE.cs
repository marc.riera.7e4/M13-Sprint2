﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class COLORLERPPRE : MonoBehaviour
{
    SpriteRenderer sp;
    [SerializeField] [Range(0f, 1f)] float lerpTime;
    [SerializeField] Color myColors;

    void Start()
    {
        sp = GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        sp.material.color = Color.Lerp(sp.material.color, myColors, lerpTime);
    }
}

